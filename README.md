# Mirror Updater

gitlab-ci mirrors hosted on gitlab.com update every 30 minutes, but can be
triggered up to every 5 minutes using the gitlab API.

This repo contains a [.gitlab-ci.yml](.gitlab-ci.yml) file which forces a
mirror update for a particular repo every time its run.

## Usage

1. Set the GITLAB_PROJECT_ID in .gitlab-ci.yml. This ID can be found at
the homepage of a gitlab repository, right below its name.

2. Set GITLAB_API_TOKEN to a secret token value of a user who has access API
access to the mirror repository.

3. Go to CI/CD -> Schedules and create a schedule to run every 5m ("*/5 * * * *").
